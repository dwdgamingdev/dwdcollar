package net.downwithdestruction.dwdcollar;

import java.util.logging.Level;

import net.downwithdestruction.dwdcollar.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class DwDCollar extends JavaPlugin {
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		
		Bukkit.getLogger().log(Level.INFO, "v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		Bukkit.getLogger().log(Level.INFO, "Plugin Disabled.");
	}
}
