package net.downwithdestruction.dwdcollar.listeners;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class PlayerListener implements Listener {
	@EventHandler(priority = EventPriority.LOWEST)
	public void onShopSell(PlayerInteractEntityEvent event) {
		final Wolf wolf = (Wolf) event.getRightClicked();
		final Player player = event.getPlayer();
		if (!player.getItemInHand().getType().equals(Material.STICK))
			return; // Not holding a stick
		if (!wolf.getType().equals(EntityType.WOLF))
			return; // Not a wolf
		if (!wolf.isTamed())
			return; // Not tamed
		final DyeColor currentColor = wolf.getCollarColor();
		DyeColor nextColor = null;
		final DyeColor[] colors = DyeColor.values();
		for (int i = 0; i < colors.length; i++) {
			if (colors[i].equals(currentColor)) {
				try {
					nextColor = colors[i+1];
				} catch(ArrayIndexOutOfBoundsException e) {
					nextColor = colors[0];
				}
				break;
			}
		}
		if (nextColor == null)
			return; // Something went wrong
		wolf.setCollarColor(nextColor);
		event.setCancelled(true); // Stops the wolf from sitting/standing
	}
}
